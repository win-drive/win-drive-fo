import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import FileUpload from '../models/FileUpload';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  authorization = {
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYXJ0aW4yIiwiZXhwIjoxNjU0MTc1NTY5fQ.ZLNU3pXQsG04Q1qvGQ1hsu6DrnPR3DQBR-6ZN17f2eJbknC9dCQYTRjkWOL4aA1BKXKkpMOf1LxzR_q3n0iyMg'
  };

  constructor(private https: HttpClient) { }

  upload(fileUpload: FileUpload): Observable<boolean> {
    const formData: FormData = new FormData();
    formData.append('multipartFile', fileUpload.file);
    return this.https.post<boolean>('/api/storage/upload?path=' + fileUpload.parentPath, formData, { headers: new HttpHeaders(this.authorization) });
  }

}
