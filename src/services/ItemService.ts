import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import Item from '../models/Item';
import TreeItem from '../models/TreeItem';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  authorization = {
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYXJ0aW4yIiwiZXhwIjoxNjU0MTc1NTY5fQ.ZLNU3pXQsG04Q1qvGQ1hsu6DrnPR3DQBR-6ZN17f2eJbknC9dCQYTRjkWOL4aA1BKXKkpMOf1LxzR_q3n0iyMg'
  };

  constructor(private https: HttpClient) { }

  getTree(): Observable<TreeItem> {
    return this.https.get<TreeItem>('/api/files/tree', { headers: new HttpHeaders(this.authorization) });
  }

  getAll(): Observable<Item[]> {
    return this.https.get<Item[]>('/api/files', { headers: new HttpHeaders(this.authorization) });
  }

}
