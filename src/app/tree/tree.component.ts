import { Component, Input } from '@angular/core';
import TreeItem from '../../models/TreeItem';
import { faAngleRight, faAngleDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})
export class TreeComponent {

  @Input() tree: TreeItem;

  faAngleRight = faAngleRight;
  faAngleDown = faAngleDown;

  opened = false;

}
