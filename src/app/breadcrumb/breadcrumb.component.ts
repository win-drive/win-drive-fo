import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent {

  @Input() path: string;

  faArrowLeft = faArrowLeft;
  faArrowRight = faArrowRight;

}
