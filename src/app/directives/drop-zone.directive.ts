import { Directive, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';
import FileUpload from '../../models/FileUpload';

// Angular Drag and Drop File
//
// Add this directive to an element to turn it into a dropzone
// for drag and drop of files.
// Example:
//
// <div (appDropZone)="onDrop($event)"></div>
//
// Any files dropped onto the region are then
// returned as a Javascript array of file objects.
// Which in TypeScript is `Array<File>`
//

@Directive({
  selector: '[appDropZone]'
})
export class DropZoneDirective {

  // The directive emits a `fileDrop` event
  // with the list of files dropped on the element
  // as an JS array of `File` objects.
  @Output('appDropZone') fileDrop = new EventEmitter<FileUpload[]>();

  // Disable dropping on the body of the document.
  // This prevents the browser from loading the dropped files
  // using it's default behaviour if the user misses the drop zone.
  // Set this input to false if you want the browser default behaviour.
  @Input() preventBodyDrop = true;

  // The `drop-zone-active` class is applied to the host
  // element when a drag is currently over the target.
  @HostBinding('class.drop-zone-active')
  active = false;

  @HostListener('drop', ['$event'])
  async onDrop(event: DragEvent) {
    event.preventDefault();
    this.active = false;

    const items = event.dataTransfer?.items;
    if (!items) return;

    const filesUpload: FileUpload[] = [];

    for (let i = 0; i < items.length; i++) {
      const item = items[i].webkitGetAsEntry();
      if (!item) continue;
      const fileUpload = this.scanFiles(item)
      console.log(fileUpload)
      if(fileUpload) filesUpload.push(fileUpload);
    }

    this.fileDrop.emit(filesUpload);
  }

  scanFiles(item: FileSystemEntry): FileUpload | null {
    if (item.isDirectory) {
      const directory = item as FileSystemDirectoryEntry;
      const directoryReader = directory.createReader();

      directoryReader.readEntries(entries => {
        entries.forEach(entry => {
          this.scanFiles(entry);
        });
      });
    }
    if (item.isFile) {
      const fileEntry = item as FileSystemFileEntry;
      const path = fileEntry.fullPath.substring(0, fileEntry.fullPath.lastIndexOf('/'));
      fileEntry.file(file => {
        return {
          parentPath: path,
          file: file
        };
      });
    }
    return null;
  }

  @HostListener('dragover', ['$event'])
  onDragOver(event: DragEvent) {
    event.stopPropagation();
    event.preventDefault();
    this.active = true;
  }

  @HostListener('dragleave', ['$event'])
  onDragLeave() {
    this.active = false;
  }

  @HostListener('body:dragover', ['$event'])
  onBodyDragOver(event: DragEvent) {
    if (this.preventBodyDrop) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  @HostListener('body:drop', ['$event'])
  onBodyDrop(event: DragEvent) {
    if (this.preventBodyDrop) {
      event.preventDefault();
    }
  }
}
