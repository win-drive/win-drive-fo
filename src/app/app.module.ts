import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ItemService } from '../services/ItemService';
import { StorageService } from '../services/StorageService';
import { DropZoneDirective } from './directives/drop-zone.directive';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ItemComponent } from './item/item.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { FormsModule } from '@angular/forms';
import { TreeComponent } from './tree/tree.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DropZoneDirective,
    ItemComponent,
    BreadcrumbComponent,
    TreeComponent
  ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        DragDropModule,
        HttpClientModule,
        FontAwesomeModule,
        FormsModule
    ],
  providers: [
    ItemService,
    StorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
