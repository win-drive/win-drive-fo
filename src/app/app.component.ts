import { Component, OnInit } from '@angular/core';
import { StorageService } from '../services/StorageService';
import { ItemService } from '../services/ItemService';
import Item from '../models/Item';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { faFolder } from '@fortawesome/free-solid-svg-icons';
import FileUpload from '../models/FileUpload';
import TreeItem from '../models/TreeItem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  faFile = faFile;
  faFolder = faFolder;

  path = '/test/images/video/music/mon fichier/lol/mdr/ptdr'
  title = 'drive-fo';

  tree: TreeItem;
  files: Item[] = [];

  constructor(private uploadService: StorageService, private fileService: ItemService) {}

  ngOnInit() {
    //this.fileService.getAll().subscribe(files => this.files = files.sort(a => { return a.isDirectory ? -1 : 1; }));
    this.fileService.getTree().subscribe(treeItem => this.tree = treeItem);
  }

  onDrop(files: FileUpload[]) {
    console.log(files);
    files.forEach((fileUpload) => {
      this.uploadService.upload(fileUpload).subscribe({
        next: (data) => {
          if (data) {
            console.log('UPLOAD - SUCCESS - ' + fileUpload.file.name);
            this.files.push({
              name: fileUpload.file.name,
              lastModified: fileUpload.file.lastModified.toString(),
              size: fileUpload.file.size,
              isDirectory: false,
              path: fileUpload.parentPath
            });
          }
          else console.log('UPLOAD - ERROR - ' + fileUpload.file.name);
        },
        error: () => {
          console.log('UPLOAD - ERROR - ' + fileUpload.file.name);
        }
      });
    });
  }

  convertByte(number: number): string {
    if(number >= 1_125_899_906_842_624) return Math.round(number / 1_125_899_906_842_624 * 10) / 10 + ' Po';
    if(number >= 1_099_511_627_776) return Math.round(number / 1_099_511_627_776 * 10) / 10 + ' To';
    if(number >= 1_073_741_824) return Math.round(number / 1_073_741_824 * 10) / 10 + ' Go';
    if(number >= 1_048_576) return Math.round(number / 1_048_576 * 10) / 10 + ' Mo';
    if(number >= 1024) return Math.round(number / 1024 * 10) / 10 + ' Ko';
    return number + ' octets';
  }

  test() {
    console.log(this.path)
  }

  onItemClick(item: Item) {
    if(item.isDirectory) {
      console.log("OUIIIIII")
      this.path += '/' + item.name;
    }else {
      console.log("nonnn")
    }
  }

}
