export default class Item {

  name: string;
  lastModified: string;
  size: number
  isDirectory: boolean;
  path: string;

}
