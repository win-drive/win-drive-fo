export default class TreeItem {

  name: string;
  isDirectory: boolean;
  children: TreeItem[] = [];

}
